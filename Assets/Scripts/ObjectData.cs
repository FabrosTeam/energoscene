﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ObjectData : MonoBehaviour
{
	[SerializeField]
	private	Text textField;
	[SerializeField]
	private Animator anim;

	public void SetText(string text)
	{
		textField.text = text;
	}


	public void SetAnimation(bool isEnable)
	{
		anim.SetBool("In", isEnable);
	}

}
