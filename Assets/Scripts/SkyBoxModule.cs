﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxModule : MonoBehaviour
{

	public Material skyBoxMaterial;

	LensFlare flare;
	Light directionalLight;

	public float fullDay = 120f; // сколько длиться день, в секундах
	[Range(0, 1)] public float currentTime;


	// Start is called before the first frame update
	void Start()
    {
		flare = GetComponent<LensFlare>();
		directionalLight = GetComponent<Light>();

		directionalLight.intensity = 0.0f;
		flare.brightness = 0.0f;

		skyBoxMaterial.SetFloat("_Exposure", 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
		currentTime += Time.deltaTime / fullDay;

		if (currentTime >= 1) currentTime = 0; else if (currentTime < 0) currentTime = 0;

		if (currentTime <= 0.5f)
		{
			directionalLight.intensity += 1.0f / fullDay;
			print(directionalLight.intensity);

		}
		else if (currentTime >= 0.5f)
		{
			directionalLight.intensity += 1.4f / fullDay;
		}



	}
}
