﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableModel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		StartCoroutine(Delay());
    }

	IEnumerator Delay()
	{
		yield return new WaitForSeconds(0.2f);
		this.gameObject.SetActive(false);
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
