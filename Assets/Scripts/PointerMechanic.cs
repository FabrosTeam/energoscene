﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class PointerMechanic : MonoBehaviour
{
	VRTK_Pointer pointer;

	Transform highlited;
	public AudioClip clip;

	private void Awake()
	{
		pointer = GetComponent<VRTK_Pointer>();

		pointer.PointerStateValid += FindedObject;

		pointer.PointerStateInvalid += UnfindedObject;

		pointer.DestinationMarkerExit += ExitMarker;
		pointer.DestinationMarkerEnter += EnterMarker;
		pointer.ActivationButtonPressed += SoundPlay;

	}

	private void SoundPlay(object sender, ControllerInteractionEventArgs e)
	{
		this.GetComponent<AudioSource>().clip = clip;
		this.GetComponent<AudioSource>().Play();
	}

	private void EnterMarker(object sender, DestinationMarkerEventArgs e)
	{
		if (e.target.GetComponent<OutData>())
		{
			InteractObjectHighlighterEventArgs temp = new InteractObjectHighlighterEventArgs();
			temp.affectingObject = this.gameObject;

			e.target.GetComponent<OutData>().OutDataOnTheUI(this, temp);

			if (highlited == null) highlited = e.target;

			e.target.GetComponent<VRTK_InteractObjectHighlighter>().Highlight(Color.yellow);
		}
	}

	private void ExitMarker(object sender, DestinationMarkerEventArgs e)
	{
		if (highlited != null)
		{
			InteractObjectHighlighterEventArgs temp = new InteractObjectHighlighterEventArgs();
			temp.affectingObject = this.gameObject;

			e.target.GetComponent<OutData>().CloseTab(this, temp);

			highlited.GetComponent<VRTK_InteractObjectHighlighter>().Unhighlight();

			highlited = null;
		}
	}

	private void UnfindedObject(object sender, DestinationMarkerEventArgs e)
	{
		if (highlited != null)
		{
			InteractObjectHighlighterEventArgs temp = new InteractObjectHighlighterEventArgs();
			temp.affectingObject = this.gameObject;

			e.target.GetComponent<OutData>().CloseTab(this, temp);
			highlited.GetComponent<VRTK_InteractObjectHighlighter>().Unhighlight();

			highlited = null;
		}
	}

	private void FindedObject(object sender, DestinationMarkerEventArgs e)
	{
		if (e.target.GetComponent<OutData>())
		{
			InteractObjectHighlighterEventArgs temp = new InteractObjectHighlighterEventArgs();
			temp.affectingObject = this.gameObject;

			e.target.GetComponent<OutData>().OutDataOnTheUI(this, temp);


			if (highlited == null) highlited = e.target;
			e.target.GetComponent<VRTK_InteractObjectHighlighter>().Highlight(Color.yellow);
		}

	}
}
